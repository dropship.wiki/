#! /usr/bin/env ruby -w

require "logger"
require "digest"
require "sqlite3"
require "sinatra"
require "netaddr"

ALLOW_USERS = {:filipp => '1234', :user1 => '1234'}
ALLOW_UPLOADS_FROM = NetAddr::CIDR.create("10.11.0.0/24")
ALLOW_DOWNLOADS_FROM = NetAddr::CIDR.create("0.0.0.0/0")

log = Logger.new(STDOUT)
log.level = Logger::DEBUG
db = SQLite3::Database.new("dropship.db")

class Shipment
    def upload
    end
    def download
    end
end


get '/get/:hash' do |h|
    unless ALLOW_DOWNLOADS_FROM.contains?(request.ip)
        status 403
        return "You are not allowed to download from here"
    end
    found = false
    db.results_as_hash = true
    db.execute( "SELECT * FROM uploads WHERE hash = ?", [h] ) do |row|
        found = true
        send_file "uploads/#{row['hash']}.data", :type => :pdf, :filename => row['filename']
        db.execute( "INSERT INTO downloads (hash, ts) VALUES (?, DATETIME())", [h] )
    end
    unless found
        status 404
        return "The requested file was not found on this server."
    end
end

post '/upload' do
    unless ALLOW_UPLOADS_FROM.contains?(request.ip)
        status 403
        return "You are not allowed to upload here"
    end

    fd = params['file'][:tempfile].read
    hash = Digest::SHA256.new.update(fd).to_s
    path = "uploads/#{hash}.data"

    if File.exists? path
        return "This file has already been uploaded"
    end

    File.open(path, "w") do |f|
        f.write(fd)
    end

    db.execute("INSERT INTO uploads (sender, hash, filename, ts) VALUES (?, ?, ?, DATETIME())",
        [request.ip, hash, params['file'][:filename]])

    return "The file was successfully uploaded!"
end

get '/' do
    unless ALLOW_DOWNLOADS_FROM.contains?(request.ip)
        status 403
        return "You are not allowed to browse here"
    end
    db.results_as_hash = true
    @files = db.execute("SELECT * FROM uploads")
    erb :index
end
